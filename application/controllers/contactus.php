<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contactus
 *
 * @author arsan
 */
class Contactus extends CI_Controller {

    public function index($lang_code = "", $isSuccess = "") {
        $langData = $this->mothermodel->getLangData($lang_code);
        $data = array();
        $data['langData'] = $langData;
        $dataContent = array();
        $dataContent['langData'] = $langData;
        $data['content'] = $this->load->view('contactus', $dataContent, true);
        $data['page'] = "contactus";
        $this->load->view('masterpage', $data);
    }

    public function send($lang_code = "") {
        $langData = $this->mothermodel->getLangData($lang_code);

        $fullname = $_POST['fullname'];
        $telephone = $_POST['telephone'];
        $email = $_POST['email'];
        $message = $_POST['message'];

        $data = array(
            'mother_shop_id' => 1,
            'parent_id' => 1,
            'recursive_id' => 0,
            'sort_priority' => 1,
            'enable_status' => 'show',
            'create_by' => 0,
            'update_by' => 0,
            'name' => $fullname,
            'telephone' => $telephone,
            'email' => $email,
            'message' => $message
        );
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->insert('tbl_contactus_form', $data);
        redirect('contactus/index/' . $langData->lang_code . '/true');
    }

}
