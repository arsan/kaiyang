<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of news
 *
 * @author arsan
 */
class News extends CI_Controller {

    public function index($lang_code = "", $content_id = 0) {
        $langData = $this->mothermodel->getLangData($lang_code);
        $data = array();
        $data['langData'] = $langData;
        $dataContent = array();
        $dataContent['langData'] = $langData;
        if(!is_numeric($content_id) || $content_id < 1){
            $content_id = $this->mothermodel->getDefaultContentId(6);
        }
        $dataContent['contentId'] = $content_id;
        $data['content'] = $this->load->view('news', $dataContent, true);
        $data['page'] = "news";
        $this->load->view('masterpage', $data);
    }

}
