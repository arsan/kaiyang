<div class="home-content clearfix">
    <div class="about-content">
        <div class="logo">
            <img src="<?php echo base_url('assets/images/home/rama9kaiyang.png'); ?>"/>
        </div>
        <div class="about">
            <?php echo nl2br($this->mothermodel->getStaticContent(1, $langData->lang_id)->about); ?>
        </div>
        <div class="icon">
            <img src="<?php echo base_url('assets/images/home/icon.png'); ?>"/>
        </div>
    </div>
</div>