<div class="popup_container white-popup-block">
    <?php
    $row = $this->mothermodel->getDynamicSingleContent(2, $langData->lang_id, $contentId);
    ?>
    <img src = "<?php echo $row->popup; ?>" class="bg"/>
    <div class="content">
        <h2><?php echo $row->title; ?></h2>
        <p><?php echo nl2br($row->detail); ?></p>
    </div>
</div>