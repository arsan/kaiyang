<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 *
 * @author arsan
 */
class Menu extends CI_Controller {

    public function index($lang_code = "") {
        $langData = $this->mothermodel->getLangData($lang_code);
        $data = array();
        $data['langData'] = $langData;
        $dataContent = array();
        $dataContent['langData'] = $langData;
        $data['content'] = $this->load->view('menu', $dataContent, true);
        $data['page'] = "menu";
        $this->load->view('masterpage', $data);
    }

}
