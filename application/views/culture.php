<div class="culture-content clearfix">
    <div class="culture-content-inner">
        <ul class="bxslider clearfix">
            <?php
            $query = $this->mothermodel->getDynamicContent(5, $langData->lang_id);
            foreach ($query->result() as $row) {
                ?>
                <li>
                    <img src="<?php echo base_url('../' . $row->thumb); ?>"/>
                    <div class="caption">
                        <div class="title clearfix"> <label><?php echo $row->title; ?></label> <a href="<?php echo base_url('index.php/culture/popup/' . $langData->lang_code . '/' . $row->culture_id); ?>" class="readmore ajax-popup-link"><img src="<?php echo base_url('assets/images/recommend/readmore.png'); ?>"/></a></div>
                        <div class="detail">
                            <?php echo nl2br($row->sub_title); ?>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.bxslider').bxSlider({
            minSlides: 4,
            maxSlides: 4,
            slideWidth: 229,
            slideMargin: 0,
            moveSlides:1,
            onSlideAfter: function() {
                $('.ajax-popup-link').magnificPopup({
                    type: 'ajax'
                });
            }
        });
        $('.ajax-popup-link').magnificPopup({
            type: 'ajax'
        });
    });
</script>