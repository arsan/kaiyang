<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of recommend
 *
 * @author arsan
 */
class Recommend extends CI_Controller {

    public function index($lang_code = "") {
        $langData = $this->mothermodel->getLangData($lang_code);
        $data = array();
        $data['langData'] = $langData;
        $dataContent = array();
        $dataContent['langData'] = $langData;
        $data['content'] = $this->load->view('recommend', $dataContent, true);
        $data['page'] = "recommend";
        $this->load->view('masterpage', $data);
    }

    public function popup($lang_code = "", $content_id = 0) {
        $langData = $this->mothermodel->getLangData($lang_code);
        $data = array();
        $data['langData'] = $langData;
        $data['contentId'] = $content_id;
        $this->load->view('recommend_popup', $data);
    }

}
