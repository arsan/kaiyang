<div class="contactus-content clearfix">
    <div class="contactus-content-inner">
        <div class="contactus-content-left">
            <img src="<?php echo base_url('../'.$this->mothermodel->getStaticContent(7, $langData->lang_id)->image_map); ?>"/>
            <a href="<?php echo $this->mothermodel->getStaticContent(7, $langData->lang_id)->google_map;?>" target="_blank" class="google-map"><img src="<?php echo base_url('assets/images/contactus/google-map.png'); ?>"/></a>
        </div>
        <div class="contactus-content-right">
            <address>
                <?php echo nl2br($this->mothermodel->getStaticContent(7, $langData->lang_id)->address); ?>
            </address>
            <h2>CONTACT FORM</h2>
            <form method="post" action="<?php echo base_url('index.php/contactus/send/'.$langData->lang_code); ?>">
                <div class="formgroup">
                    <label>
                        Name:*
                    </label>
                    <input type="text" name="fullname"/>
                </div>
                <div class="formgroup">
                    <label>
                        Telephone:*
                    </label>
                    <input type="text" name="telephone"/>
                </div>
                <div class="formgroup">
                    <label>
                        Email:*
                    </label>
                    <input type="text" name="email"/>
                </div>
                <div class="formgroup">
                    <label>
                        Message:*
                    </label>
                    <textarea name="message"></textarea>
                </div>
                <div class="formgroup">
                    <label>

                    </label>
                    <small>* Important Infomation</small> <input type="submit" class="btn-submit"/>
                </div>
            </form>
        </div>
    </div>
</div>