<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fonts/stylesheet.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/styles.css'); ?>">
        <!-- jQuery library (served from Google) -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/perfect-scrollbar/src/jquery.mousewheel.js'); ?>"></script>
        <!-- bxSlider Javascript file -->
        <script src="<?php echo base_url('assets/jquery.bxslider/jquery.bxslider.min.js'); ?>"></script>
        <!-- bxSlider CSS file -->
        <link href="<?php echo base_url('assets/jquery.bxslider/jquery.bxslider.css'); ?>" rel="stylesheet" />
        <!-- perfect-scrollbar Javascript file -->
        <script src="<?php echo base_url('assets/perfect-scrollbar/src/perfect-scrollbar.js'); ?>"></script>
        <!-- perfect-scrollbar CSS file -->
        <link href="<?php echo base_url('assets/perfect-scrollbar/src/perfect-scrollbar.css'); ?>" rel="stylesheet" />
        <!-- Magnific Popup core JS file -->
        <script src="<?php echo base_url('assets/jquery.magnific-popup/jquery.magnific-popup.min.js'); ?>"></script> 
        <!-- Magnific Popup core CSS file -->
        <link rel="stylesheet" href="<?php echo base_url('assets/jquery.magnific-popup/magnific-popup.css'); ?>"> 
    </head>
    <body>
        <div class="wrapper">
            <header class="clearfix">
                <div class="logo">
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="พระราม9 ไก่ย่าง"/></a>
                </div>
                <nav>
                    <ul class="clearfix">
                        <li class="<?php echo ($page == 'home') ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('index.php/home/index/' . $langData->lang_code); ?>">HOME</a>
                        </li>
                        <li class="<?php echo ($page == 'recommend') ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('index.php/recommend/index/' . $langData->lang_code); ?>">RECOMMEND</a>
                        </li>
                        <li class="<?php echo ($page == 'menu') ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('index.php/menu/index/' . $langData->lang_code); ?>">MENU</a>
                        </li>
                        <li class="<?php echo ($page == 'culture') ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('index.php/culture/index/' . $langData->lang_code); ?>">CULTURE</a>
                        </li>
                        <li class="<?php echo ($page == 'news') ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('index.php/news/index/' . $langData->lang_code); ?>">NEWS</a>
                        </li>
                        <li class="<?php echo ($page == 'contactus') ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('index.php/contactus/index/' . $langData->lang_code); ?>">CONTACT US</a>
                        </li>
                    </ul>
                </nav>
                <div class="lang">
                    <label>Language:</label>
                    <?php
                    $segment_1 = 'home/';
                    if ($this->uri->slash_segment(1) != '/') {
                        $segment_1 = $this->uri->slash_segment(1);
                    }
                    $segment_2 = 'index/';
                    if ($this->uri->slash_segment(2) != '/') {
                        $segment_2 = $this->uri->slash_segment(2);
                    }
                    ?>
                    <a href="<?php echo base_url('index.php/' . $segment_1 . $segment_2 . 'TH/'); ?>" class="<?php echo ($langData->lang_code == 'TH')?'active':'';?>">TH</a>
                    <a href="<?php echo base_url('index.php/' . $segment_1 . $segment_2 . 'EN/'); ?>" class="<?php echo ($langData->lang_code == 'EN')?'active':'';?>">EN</a>
                </div>
            </header>
            <div class="main-content">
                <?php echo $content; ?>
            </div>
            <footer class="clearfix">
                <div class="tel">
                    <a href="tel:0-2719-8039">0-2719-8039</a>   <a href="tel:0-2719-9139 Ext 1">0-2719-9139 Ext 1</a>   <a href="tel:081-447-3897">081-447-3897</a>
                </div>
                <div class="copyright">
                    2014 © Praram 9 Kaiyang All Rights Reserved
                </div>
            </footer>
        </div>
    </body>
</html>
