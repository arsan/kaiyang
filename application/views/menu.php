<div class="menu-content clearfix">
    <div class="menu-content-inner contentHolder" id="contentScrollbar">
        <div class="content">
            <ul>
                <?php
                $query = $this->mothermodel->getDynamicContent(3, $langData->lang_id);
                foreach ($query->result() as $row) {
                    ?>
                    <li>
                        <h3>
                            <?php echo $row->group_name; ?>
                        </h3>
                        <ul>
                            <?php
                            $query2 = $this->mothermodel->getDynamicContent(4, $langData->lang_id, $row->menu_id);
                            foreach ($query2->result() as $row2) {
                                ?>
                                <li><?php echo $row2->title; ?> <label class="price"><?php echo $row2->price; ?></label></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<script>
    $(document).ready(function($) {
        $('#contentScrollbar').perfectScrollbar();
    });
</script>