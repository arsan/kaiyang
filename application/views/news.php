<div class="news-content clearfix">
    <div class="news-content-left">
        <?php $row = $this->mothermodel->getDynamicSingleContent(6, $langData->lang_id, $contentId); ?>
        <div class="news-content-left-image">
            <img src="<?php echo base_url('../'.$row->photo); ?>"/>
        </div>
        <div class="news-content-left-detail">
            <div class="news-content-left-detail-title">
                <?php echo $row->title;?>
            </div>
            <p>
                <?php echo nl2br($row->detail);?>
            </p>
        </div>
    </div>
    <div class="news-content-right contentHolder" id="contentScrollbar">
        <div class="content">
            <ul>
                <?php
                $query = $this->mothermodel->getDynamicContent(6, $langData->lang_id);
                foreach ($query->result() as $row) {
                    ?>
                    <li class="clearfix">
                        <a href="<?php echo base_url('index.php/news/index/' . $langData->lang_code . '/' . $row->news_id); ?>">
                            <img src="<?php echo base_url('../' . $row->thumb); ?>"/>
                        </a>
                        <div>
                            <h3><a href="<?php echo base_url('index.php/news/index/' . $langData->lang_code . '/' . $row->news_id); ?>"><?php echo $row->title; ?></a></h3>
                            <p>
                                <a href="<?php echo base_url('index.php/news/index/' . $langData->lang_code . '/' . $row->news_id); ?>">
                                    <?php echo nl2br($row->sub_title); ?>
                                </a>
                            </p>
                            </a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<script>
    $(document).ready(function($) {
        $('#contentScrollbar').perfectScrollbar();
    });
</script>